const getSum = (str1, str2) => {
    if (typeof str1 !== 'string' || typeof str2 !== 'string') return false;
    if (str1.length === 0) str1 = '0';
    if (str2.length === 0) str2 = '0';
    const REGEX = /^\d+$/;
    if (!REGEX.test(str1) || !REGEX.test(str2)) return false;
    const SUM = parseInt(str1) + parseInt(str2);
    return SUM.toString();  

};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let books = 0,
    comments = 0;

  for (let i in listOfPosts) {
    if (listOfPosts[i]['author'] === authorName) books++;
    for (let j in listOfPosts[i]['comments'])
      if (listOfPosts[i]['comments'][j]['author'] === authorName) comments++;
  }

  return `Post:${books},comments:${comments}`;

};

const tickets=(people)=> {
    var a25 = 0, a50 = 0;
    for (var i = 0; i < people.length; i++) {
        if (people[i] == 25) {
            a25 += 1;
        }
        if (people[i] == 50) {
            a25 -= 1; a50 += 1;
        }
        if (people[i] == 100) {
            if (a50 == 0 && a25 >= 3) {
                a25 -= 3;
            } else {
                a25 -= 1; a50 -= 1;
            }
        }
        if (a25 < 0 || a50 < 0) {
            return 'NO';
        }
    }
    return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};